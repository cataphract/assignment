<?php

/**
 * Interface exposing a method for calculating a tax based only on the tax base.
 * Any other variables will have to depend on the implementing object type or
 * state.
 */
interface TaxCalculator {

	/**
	 * Computes the tax.
	 * @param $base The tax base, in cents.
	 */
	function computeTax($base);
}
