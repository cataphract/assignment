<?php

/**
 * Implements a tax calculation scheme where the tax base is successively
 * handled to several objects, which may add to the total or reduce the tax
 * base.
 */
class ChainedTax implements TaxCalculator {
	/** @var TaxPart[] */
	private $chain;

	/**
	 * @param TaxPart[] The parts, in the order they should be applied.
	 */
	public function __construct($chain) {
		$this->chain = $chain;
	}

	public function computeTax($baseArg) {
		$tax  = 0;
		$base = $baseArg;
		foreach ($this->chain as $c) {
			$res   = $c->doPart($base);
			$tax  += $res[0];
			$base  = $res[1];
		}
		return $tax;
	}
}
