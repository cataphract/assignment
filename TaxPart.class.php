<?php

interface TaxPart {

	/**
	 * Add a new amount to the tax, optionally changing the tax base.
	 * @return array Numeric array. First element is the amount of tax to be
	 *				 added, second is the new base.
	 */
	function doPart($base);
}
