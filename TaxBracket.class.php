<?php

class TaxBracket implements TaxPart {

	private $lowerBound;
	private $rate;

	/**
	 * @param int $lowerBound The bracket lower bound, in cents.
	 * @param int $rate		  The tax rate * 100
	 */
	public function __construct($lowerBound, $rate) {
		$this->lowerBound = $lowerBound;
		$this->rate = $rate;
	}

	public function doPart($base) {
		$relAmount = max(0, $base - $this->lowerBound);
		$tax = (int)floor(($this->rate * $relAmount) / (10000));
		return array($tax, $base - $relAmount);
	}
}
