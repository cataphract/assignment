<?php

class TaxBracketTest extends PHPUnit_Framework_TestCase {
	
	public function provider() {
		return [
			[7000, 6000, 3333, [333, 6000]],
			[6000, 7000, 5000, [0,   6000]],
			[0,    5000, 5000, [0,   0]],
			[1000, 0,    7500, [750, 0]],
		];
	}

	/**
	 * @dataProvider provider
	 */
	public function testDoPart($base, $lowerBound, $rate, $exp) {
		$tb = new TaxBracket($lowerBound, $rate);
		$this->assertEquals($exp, $tb->doPart($base));
	}

}
