<?php

class IncomeTaxFactoryTest extends PHPUnit_Framework_TestCase {

	public function testBuildIncomeTaxCalculator() {
		//the only guarentee the IncomeTaxFactory gives is that it returns a TaxCalculator
		//the rest are implementation details
		$calc = IncomeTaxFactory::buildIncomeTaxCalculator(60);
		$this->assertInstanceOf('TaxCalculator', $calc);
	}
}

