<?php

class ChainedTaxTest extends PHPUnit_Framework_TestCase {

	public function testComputeTax() {
		$mock1 = $this->getMock('TaxPart', ['doPart']);
		$mock1->expects($this->once())
		      ->method('doPart')
		      ->with($this->equalTo(1100))
		      ->will($this->returnValue(array(33, 1000)));
		$mock2 = $this->getMock('TaxPart', ['doPart']);
		$mock2->expects($this->once())
		      ->method('doPart')
		      ->with($this->equalTo(1000))
		      ->will($this->returnValue(array(66, 0)));
		$ct = new ChainedTax([$mock1, $mock2]);
		$this->assertEquals(99, $ct->computeTax(1100));
	}

}
