<?php

class IntegrationTest extends PHPUnit_Framework_TestCase {

	public function provider() {
		return [
			[20, 6000000, 2419840],
			[80, 6000000, 1833829],
			[20, 2000000, 684146],
			[80, 2000000, 326146],
		];
	}

	/**
	 * @dataProvider provider
	 */
	public function testIntegration($age, $base, $exp) {
		$tc = IncomeTaxFactory::buildIncomeTaxCalculator($age);
		$this->assertEquals($exp, $tc->computeTax($base));
	}
}
