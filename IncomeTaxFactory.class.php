<?php

abstract class IncomeTaxFactory {
	private static $BRACKETS = array(
		5436700,
		3273800,
		1821800,
		0
	);
	private static $REGULAR_RATES = array(
		5200,
		4200,
		4195,
		3345
	);
	private static $ELDER_RATES = array(
		5200,
		4200,
		2405,
		1555
	);

	/**
	 * @param int $age The age of the subject, in years.
	 * @return TaxCalculator
	 */
	public static function buildIncomeTaxCalculator($age) {
		$rates = ($age < 65) ? self::$REGULAR_RATES : self::$ELDER_RATES;
		return new ChainedTax(
			array_map(function($lowerBound, $rate) {
					return new TaxBracket($lowerBound, $rate);
				},
				self::$BRACKETS,
				$rates)
		);
	}
}
